
function elementos() {

    for (let i = 0; i < 5; i++) {

        let num = Math.round(Math.random() * 100 + 1)
            
        if ((num % 3) == 0 && (num % 5) == 0) {
            console.log(`${num} fizzbuzz` )
        } else if ((num % 3) == 0) {
            console.log(`${num} fizz` )

        } else if ((num % 5) == 0 ) {
            console.log(`${num} buzz` )
        } else {
            console.log(`${num} nada` )
        }
    }
    
}